git clone --bare http://gitlab.com/Czichy/dotfiles.git $HOME/.cfg

/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME checkout --force

/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME config --local status.showUntrackedFiles no


#Mount Homeserver
//192.168.1.80/Dokumente /home/czichy/Dokumente           cifs      noauto,nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10,credentials=/home/czichy/.credentials/smbcredentials,users,uid=1000,gid=1000,_netdev 0 0
//192.168.1.80/Bibliothek /home/czichy/Bibliothek         cifs      noauto,nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10,credentials=/home/czichy/.credentials/smbcredentials,users,uid=1000,gid=1000,_netdev 0 0
//192.168.1.80/Media /home/czichy/Media                   cifs      noauto,nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10,credentials=/home/czichy/.credentials/smbcredentials,users,uid=1000,gid=1000,_netdev 0 0
//192.168.1.80/Public /home/czichy/Public                 cifs      noauto,nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10,credentials=/home/czichy/.credentials/smbcredentials,users,uid=1000,gid=1000,_netdev 0 0
//192.168.1.80/Christian /home/czichy/Private             cifs      noauto,nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10,credentials=/home/czichy/.credentials/smbcredentials,users,uid=1000,gid=1000,_netdev 0 0
