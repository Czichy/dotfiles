#!/bin/sh

primary=DP-0
secondary=DP-2

{
    if xrandr | grep "$primary connected" && xrandr | grep "$secondary connected" ; then
        xrandr  --dpi 144 \
            --output DP-0 --primary --mode 3840x2160 --pos 0x0 --rotate normal --panning 3840x2160  \
            --output DP-1 --off \
            --output DP-2 --mode 1920x1080 --pos 3840x0 --rotate normal --scale 1.5x1.5 \
            --output DP-3 --off \
            --output HDMI-0 --off \
            --output DP-4 --off \
            --output DP-5 --off
    else xrandr | grep "$primary connected" && xrandr | grep "$secondary disconnected" ;
        xrandr --dpi 144 \
            --output DP-0 --primary --mode 3840x2160 --pos 0x0 --rotate normal --panning 3840x2160 \
            --output DP-1 --off \
            --output DP-2 --off \
            --output DP-3 --off \
            --output HDMI-0 --off \
            --output DP-4 --off \
            --output DP-5 --off
    fi
}
