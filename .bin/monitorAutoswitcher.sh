#!/usr/bin/env bash

#export SWAYSOCK="/run/user/1000/sway-ipc.1000.$(pgrep -x 'sway').sock"
export WAYLAND_DISPLAY="wayland-0"
if rg -qw connected /sys/class/drm/card0-HDMI-A-1/status; then
    #sed -i -e 's/move position 500 0/move position 774 0/g' /home/tb/.config/sway/config;
    swaymsg output eDP-1 disable;
    swaymsg output HDMI-A-1 enable resolution 1920x1080 position 0 0 
else
    #sed -i -e 's/move position 774 0/move position 500 0/g' /home/tb/.config/sway/config
    grep -q Off /sys/class/drm/card0-eDP-1/dpms && swaymsg output eDP-1 enable
    #resolution 3000x2000 position 0 0 scale 2
fi
