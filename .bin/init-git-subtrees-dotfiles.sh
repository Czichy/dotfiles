#!/bin/bash
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

config subtree add --prefix .config/awesome https://gitlab.com/Czichy/awesomewm-config.git master --squash
config subtree add --prefix .config/polybar https://gitlab.com/Czichy/polybar-config.git main --squash
config subtree add --prefix .config/nvim https://gitlab.com/Czichy/nvim-config.git master --squash
config subtree add --prefix .config/bspwm https://gitlab.com/Czichy/bspwm-config.git master --squash
config subtree add --prefix .config/sxhkd https://gitlab.com/Czichy/sxhkd-config.git master --squash
config subtree add --prefix .config/fish https://gitlab.com/Czichy/fish-config.git master --squash
config subtree add --prefix .config/i3 https://gitlab.com/Czichy/i3-config.git main --squash
config subtree add --prefix .xmonad https://gitlab.com/Czichy/xmonad-config.git main --squash
config subtree add --prefix .config/dwm https://gitlab.com/Czichy/dwm.git main --squash
#config subtree add --prefix .config/doom git@github.com:raven2cz/emacs.git main --squash
#config subtree add --prefix .config/qtile git@github.com:raven2cz/qtile-config.git main --squash
#config subtree add --prefix .config/i3 git@github.com:raven2cz/i3-config.git main --squash
#config subtree add --prefix .config/xfce4 git@github.com:raven2cz/xfce-config.git main --squash
