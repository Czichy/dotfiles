#!/bin/bash

set -e
source /home/restic/.credentials/restic
#restic -r rclone:onedrive_nas:/backup/server backup --verbose /mnt/storage/shares/

RESTIC_REPOSITORY_BASE=rclone:onedrive_nas:/backup
BACKUP_BASEPATH=/mnt/storage/shares

export GOGC=20  # HACK: Work around for restic's high memory usage https://github.com/restic/restic/issues/1988
export RESTIC_LOG_DIR="$HOME/log"
export RESTIC_LOG_FILE="$RESTIC_LOG_DIR/$(date -Iseconds).log"

export FORGET_OPTIONS="--keep-daily 7 --keep-weekly 4 --keep-monthly 12 --keep-yearly 7 --group-by host"

mkdir -p "$RESTIC_LOG_DIR"
# need to securely provide password: https://restic.readthedocs.io/en/latest/faq.html#how-can-i-specify-encryption-passwords-automatically

#Define a timestamp function
timestamp() {
date "+%b %d %Y %T %Z"
}

# insert timestamp into log
printf "\n\n"
echo "-------------------------------------------------------------------------------" | tee -a $RESTIC_LOG_FILE
echo "$(timestamp): backup_restic.sh started" | tee -a $RESTIC_LOG_FILE

for BACKUP in dokumente library media private srv
do
    echo "$(timestamp): backup $BACKUP started" | tee -a $RESTIC_LOG_FILE
    
    PASSWD="RESTIC_$BACKUP"
    export RESTIC_PASSWORD=${!PASSWD}
    export RESTIC_REPOSITORY=${RESTIC_REPOSITORY_BASE}/${BACKUP}
    # Run Backups
    restic --verbose backup -r ${RESTIC_REPOSITORY_BASE}/${BACKUP} ${BACKUP_BASEPATH}/${BACKUP} | tee -a $RESTIC_LOG_FILE
    
    # Remove snapshots according to policy
    # If run cron more frequently, might add --keep-hourly 24
    restic forget -r ${RESTIC_REPOSITORY_BASE}/${BACKUP} --prune $FORGET_OPTIONS | tee -a $RESTIC_LOG_FILE
    
    # Remove unneeded data from the repository
    restic prune -r ${RESTIC_REPOSITORY_BASE}/${BACKUP}
    
    # Check the repository for errors
    restic check -r ${RESTIC_REPOSITORY_BASE}/${BACKUP} | tee -a $RESTIC_LOG_FILE
    echo "$(timestamp): backup $BACKUP finished" | tee -a $RESTIC_LOG_FILE
done
# insert timestamp into log
printf "\n\n"
echo "-------------------------------------------------------------------------------" | tee -a $RESTIC_LOG_FILE
echo "$(timestamp): backup_restic.sh finished" | tee -a $RESTIC_LOG_FILE
