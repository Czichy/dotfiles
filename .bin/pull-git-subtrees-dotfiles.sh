#!/bin/bash
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

config subtree pull --prefix .config/awesome https://gitlab.com/Czichy/awesomewm-config.git master --squash
config subtree pull --prefix .config/polybar https://gitlab.com/Czichy/polybar-config.git main --squash
config subtree pull --prefix .config/nvim https://gitlab.com/Czichy/nvim-config.git master --squash
config subtree pull --prefix .config/bspwm https://gitlab.com/Czichy/bspwm-config.git master --squash
config subtree pull --prefix .config/sxhkd https://gitlab.com/Czichy/sxhkd-config.git master --squash
config subtree pull --prefix .config/fish https://gitlab.com/Czichy/fish-config.git master --squash
config subtree pull --prefix .config/i3 https://gitlab.com/Czichy/i3-config.git main --squash
config subtree pull --prefix .xmonad https://gitlab.com/Czichy/xmonad-config.git main --squash
config subtree pull --prefix .config/dwm https://gitlab.com/Czichy/dwm.git main --squash
