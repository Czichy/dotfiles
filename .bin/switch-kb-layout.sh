#!/bin/sh

sleep 1

# Some environment variables that need to be set in order to run `setxkbmap`
DISPLAY=":0.0"
HOME=/home/czichy
XAUTHORITY=$HOME/.Xauthority
export DISPLAY XAUTHORITY HOME

udev_action=$1
log_file="$HOME/switch-kb-layout.log"

current_date_time="`date "+%Y-%m-%d %H:%M:%S"`"

echo "${current_date_time}  action:" "${udev_action}" >> $log_file

if [ "${udev_action}" != "add" ] && [ "${udev_action}" != "remove" ]; then
    echo "${current_date_time}  Other action. Aborting. - ${udev_action}" >> $log_file
    exit 1
fi

internal_kb_layout="de"
internal_kb_variant=""

external_kb_layout="de"
external_kb_variant=""

kb_layout=""
kb_variant=""

if [ "${udev_action}" = "add" ]; then
    kb_layout=$external_kb_layout
    kb_variant=$external_kb_variant
elif [ "${udev_action}" = "remove" ]; then
    kb_layout=$internal_kb_layout
    kb_variant=$internal_kb_variant
fi

setxkbmap -layout "${kb_layout}"
echo "${current_date_time}  set layout:" "$kb_layout" >> $log_file
if [ ! -z "${kb_variant}" ]; then
    setxkbmap -variant "${kb_variant}"
    echo "${current_date_time}  set variant:" "$kb_variant" >> $log_file
#  filename_lower_camel
