#!/bin/sh

SOURCE="/mnt/storage/shares/onedrive/Bilder/Eigene Aufnahmen/"
DESTINATION="/mnt/storage/shares/media/"

#Update any photo that doesn't have DateTimeOriginal to have it based on file modify date
exiftool -overwrite_original '-datetimeoriginal<filemodifydate' -if '(not $datetimeoriginal or ($datetimeoriginal eq "0000:00:00 00:00:00"))' -r "${SOURCE}"
#
exiftool -i SYMLINKS -v -ext JPG -ext PGN -p '${directory} ${filename} ${megapixels}' "filename<createdate" "-filename<createdate" "-filename<datetimeoriginal" -if '${Megapixels#} < 2'   -d "${DESTINATION}/fotos/small/%Y/%Y-%m/%Y%m%d_%H%M%S%%-c.%%le"   -r "${SOURCE}" -i SYMLINKS -v -t
exiftool -i SYMLINKS -v -ext JPG -ext PGN "filename<filemodifydate" "-filename<createdate" "-filename<datetimeoriginal"   -d "${DESTINATION}/fotos/%Y/%Y-%m/%Y%m%d_%H%M%S%%-c.%%le"   -r "${SOURCE}" -i SYMLINKS -v -t

exiftool -i SYMLINKS -v -ext MOV -ext MPG -ext mp4 -ext 3gp "filename<filemodifydate" "-filename<createdate" "-filename<datetimeoriginal"   -d "${DESTINATION}/filme/%Y/%Y-%m/%Y%m%d_%H%M%S%%-c.%%le"   -r "${SOURCE}" -i SYMLINKS -v -t

chown christian:czichys ${DESTINATION}/fotos -R
chown christian:czichys ${DESTINATION}/filme -R

chmod -R 755 ${DESTINATION}
