#!/bin/sh
#
# Set external display and audio (if hdmi)

# Set authority - necessary because udev rule runs script as root
########### Authority ###########
USER="$(who | grep :0\) | cut -f 1 -d ' ')"
export XAUTHORITY=/home/$USER/.Xauthority
export DISPLAY=:0
########### Environment ###########
THIS_PATH=$(dirname "${BASH_SOURCE[0]}")

## List by running xrandr ##
DP="eDP-1"
VGA="VGA-1"
HDMI="HDMI-A-1"

DP_STATUS="$(bat /sys/class/drm/card1-eDP-1/status)"
VGA_STATUS="$(bat /sys/class/drm/card1-VGA-1/status)"
HDMI_STATUS="$(bat /sys/class/drm/card1-HDMI-A-1/status)"

EXTERNAL_DISPLAY=""
EXTERNAL_DISPLAY_STATUS="disconnected"
# Check to see if the external display is connected
if [ $HDMI_STATUS = connected ]; then
  swaymsg output $HDMI enable resolution 1920x1080 position 0 0
  EXTERNAL_DISPLAY_STATUS="connected"
fi
if [ $VGA_STATUS = connected ]; then
	if [ $HDMI_STATUS = connected ]; then
		swaymsg output $VGA enable resolution 1920x1080 position 1920 0
	else
		swaymsg output $VGA enable resolution 1920x1080 position 0 0
	fi
  EXTERNAL_DISPLAY_STATUS="connected"
fi

# Change display
if [ $EXTERNAL_DISPLAY_STATUS = "connected" ]; then
	swaymsg output eDP-1 disable
# Restore internal display
else
	swaymsh output eDP-1 enable resolution 1920x1080 position 0 0
fi

exit 0
