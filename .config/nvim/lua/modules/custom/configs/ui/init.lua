local config = {}

function config.zephyr_colorscheme()
    vim.cmd("colorscheme zephyr")
end

return config
