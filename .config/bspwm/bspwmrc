# Truncate a couple of common commands that are used herein.
    _bc() {
        bspc config "$@"
    }

    _check() {
        command -v "$1" > /dev/null
    }

    # Visual options
    # ==============
    #
    # Things to bear in mind about certain configurations:
    #
    # 1. single_monocle true == Does not recognise receptacles, so if you
    #    have a single window and insert a receptacle, the window will
    #    remain in monocle view.  Desired behaviour is to tile accordingly.
    # 2. borderless_monocle true == Smart borders are bad in multi-monitor
    #    setups because you cannot tell which window has focus when
    #    a monocle exists on each monitor.  Single monitor is okay.
    # 3. ignore_ewmh_focus true == This is very opinionated as it completely
    #    prevents focus stealing.  Adapt accordingly.
    # 4. pointer_modifier mod1 == enables mouse actions while holding down
    #    the Alt key (see below---bonus: the following two also work in the
    #    popular desktop environments).
    # 5. pointer_action1 move == Alt + Right Click allows you to drag
    #    a floating window.  Also works for switching the position of tiled
    #    windows, if that is your thing.
    # 6. pointer_action2 resize_side == Alt + Left Click allows you to
    #    resize the nearest side of the focused window.  Works for both
    #    tiled and floating windows.
    # 7. external_rules_command bspwm_external_rules == References the
    #    script of mine that implements window rules and some custom
    #    behaviours.  The script is also part of my dotfiles.  Window rules
    #    can be included herein, but I prefer to keep them separate.
    # 8. Colours are edited programmatically by my `tempus` script.  Do not
    #    edit them manually (edit the script instead).  These colours
    #    concern the following:
    #
    #    * normal_border_color   == unfocused windows
    #    * active_border_color   == focused window, unfocused monitor
    #    * focused_border_color  == focused window, focused monitor
    #    * presel_feedback_color == area for a manual split (preselection)

    # window management
    # -----------------
    _bc border_width                    2
    _bc window_gap                              5
    _bc split_ratio                     0.52
    _bc single_monocle                  false
    _bc borderless_monocle              true
    _bc gapless_monocle                 true
    _bc -m DP-0 top_padding         0
    _bc -m DP-2 top_padding         0
    _bc paddingless_monocle     false
    _bc focus_follows_pointer   false
    _bc pointer_follows_monitor true
    _bc pointer_modifier                mod1
    _bc pointer_action1                 move
    _bc pointer_action2                 resize_side
    _bc click_to_focus                  any
    _bc swallow_first_click     false
    _bc initial_polarity                first_child
    _bc ignore_ewmh_focus               true

    bspc config focused_border_color "#81a1c1"
    bspc config normal_border_color "#4c566a"

    bspc config remove_disabled_monitor true
    bspc config remove_unplugged_monitor true
    bspc config merge_overlapping_monitors true
    
    # autostart
    # =========


    # Load my XTerm and relevant configurations.
    [ -f "$HOME"/.Xresources ] && xrdb -I "$HOME" -merge "$HOME"/.Xresources

    # Start workspace notifications
    [ -x "$HOME/bin/wm" ] && $HOME/bin/wm &
    # Launch polybar implementation (the system panel).
    #_check polybar && "$HOME"/.config/polybar/launch.sh &
	
    # Display compositor for enabling shadow effects and transparency
    # (disable it if performance is bad---also bear in mind that I do not
    # use transparent areas in any of my interfaces).
    killall "picom"
    _check picom && picom &

    # Launch redshift
    redshift -c ~/.config/redshift.conf &

    # specify keyboard layout and compose key
    if _check setxkbmap; then
        # setxkbmap -layout 'us,gr' -option 'grp:alt_caps_toggle'
        setxkbmap -layout 'de'
        setxkbmap -option compose:menu

        # For the key chord that performs the layout switching between US
        # QWERTY and Greek see my `sxhkdrc`.  The script:
        # `own_script_current_keyboard_layout`
    fi

    bspc config external_rules_command $HOME/.config/bspwm/evil-rules.sh

    # Remove old rules
    bspc rule -r "*"

    bspc rule -a 'Emacs' state=tiled follow=on desktop='^3'

    # Firefox's Picture-in-Picture mode
    bspc rule -a 'Nightly:Toolkit' state=float follow=off sticky=on locked=on

    bspc rule -a '*:keepassxc' sticky=on state=floating hidden=on
    bspc rule -a '*:KeePassHttp: Confirm Access:*' state=floating focus=on
    keepassxc &
    
    bspc rule -a Firefox desktop=^2
    bspc rule -a Brave-browser desktop=^2 follow=on
    brave.sh &

    bspc rule -a 'sierrachart_64.exe' desktop=^5
    # SierraChart.sh &

    bspc rule -a 'sun-awt-X11-XFramePeer' desktop =^4
    bspc rule -a 'install4j-jclient-Launcher' desktop =^4
    /home/czichy/Jts/tws &
    bspc rule -a Gimp desktop=^8 state=floating follow=on

    bspc rule -a Pavucontrol state=floating
    bspc rule -a Zathura state=tiled
    bspc rule -a qalculate-gtk state=floating
    bspc rule -a 'my_float_window:Alacritty' state=floating
    bspc rule -a Kupfer.py focus=on
    bspc rule -a Screenkey manage=off

    bspc monitor DP-0 -d 1 2 3 4 5
    bspc monitor DP-2 -d 6 7 8 9 0

    ~/.bin/java_nonreparenting_wm_hack.sh &
    wmname compiz

    #hide polybar on start
    #polybar-msg cmd hide

    systemctl --user import-environment PATH
    # Avoid using systemd 'enable' feature for Emacs as it can launch Emacs server
    # before importing most all env. variables. Instead, start Emacs service
    # manually in the beginning of a new BSPWM session.
    #systemctl --user start emacs &

    # The hotkey daemon that handles all custom key bindings, including the
    # ones that control BSPWM.  No real need to check for the presence of
    # this executable, because it is a dependency of the BSPWM package.
    #_check sxhkd && sxhkd -c "$HOME"/.config/sxhkd/sxhkdrc{,_bspc} &
    systemctl --user restart sxhkd@${XDG_VTNR}.service &

