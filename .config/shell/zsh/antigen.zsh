source /usr/share/zsh/share/antigen.zsh

antigen use prezto

antigen bundles <<EOB
  robbyrussell/oh-my-zsh plugins/encode64
  robbyrussell/oh-my-zsh plugins/fancy-ctrl-z
  hlissner/zsh-autopair
  Tarrasch/zsh-bd
  zsh-users/zsh-syntax-highlighting
  zsh-users/zsh-history-substring-search
  mafredri/zsh-async
  zsh-users/zsh-autosuggestions
  Aloxaf/fzf-tab
EOB

# Load prompt configuration
antigen theme romkatv/powerlevel10k powerlevel10k
#antigen theme denysdovhan/spaceship-prompt

antigen apply
bindkey '^T' toggle-fzf-tab
