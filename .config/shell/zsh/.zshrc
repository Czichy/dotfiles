# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/shell/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# shellcheck shell=bash

# shellcheck source=./zsh/interactive
. "${XDG_CONFIG_HOME}"/shell/zsh/interactive

# To customize prompt, run `p10k configure` or edit ~/.config/shell/zsh/.p10k.zsh.
[[ ! -f ~/.config/shell/zsh/.p10k.zsh ]] || source ~/.config/shell/zsh/.p10k.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
