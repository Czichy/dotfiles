# Developer completions

# push/pop
abbr -a -U -- - prevd
abbr -a -U -- = nextd

# exit
abbr :q  'exit'
abbr :q! 'exit'

# Autojump
abbr j  'z'

# Node.js
abbr y   'yarn'
abbr yui 'yarn upgrade-interactive --latest'
abbr yt  'yarn test'

# For today (eg, `git checkout -b (today )/update`)
abbr today "date +'%Y-%m-%d'"