if type -q cargo
  #echo "✓ Cargo"
  abbr ca 'cargo'
  abbr ct 'cargo test'
  abbr car 'cargo run'
  abbr caf 'cargo fmt'
  abbr ctw 'cargo watch -c -x "test -- --nocapture --color always"'
  abbr ccd 'rustup run nightly cargo udeps --all-targets'
end
