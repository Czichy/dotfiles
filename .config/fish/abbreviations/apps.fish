if type -q docker-compose
  #echo "✓ Docker"
  abbr dc 'docker-compose'
end

if type -q paru
  #echo "✓ Pacman"
	abbr -a p 'paru'
	abbr -a up 'rank_mirrors ;paru -S archlinux-keyring; paru -Syu'

else if type -q yay
  #echo "✓ Pacman"
	abbr -a p 'yay'
	abbr -a up 'rank_mirrors ;yay -S archlinux-keyring; yay -Syu'
else if type -q trizen
  #echo "✓ Pacman"
	abbr -a p 'trizen'
	abbr -a up 'rank_mirrors ;trizen -S archlinux-keyring; trizen -Syu'
else if type -q pacman
  #echo "✓ Pacman"
	abbr -a p 'sudo pacman'
	abbr -a up 'rank_mirrors ;pacman -S archlinux-keyring; pacman -Syu'
else
  abbr -a p 'sudo apt-get install'
  abbr -a up 'sudo apt-get update ; sudo apt-get upgrade'
end

if type -q git
  #echo "✓ Git"
  source ~/.config/fish/abbreviations/git.fish
end

# Tig
if type -q tig
  #echo "✓ Tig"
  abbr tigs 'tig status'
end

if type -q exa
  #echo "✓ Exa"
  abbr -a l 'exa --header --git -l'
  abbr -a ls 'exa'
  abbr -a ll 'exa -abghHlS --icons'
  abbr -a lll 'exa -lhgr --level=3 --icons'
else
	abbr -a l 'ls'
	abbr -a ll 'ls -l'
	abbr -a lll 'ls -la'
end

if type -q nvim
  #echo "✓ Neovim"
  abbr v 'nvim'
  abbr vim 'nvim'
  abbr vi 'nvim'
  abbr n 'nvim'
end

# if type -q nvr
#   echo "✓ Neovim remote"
#   abbr v 'nvr -s -O'
#   abbr s 'nvr -s -o'
#   abbr t 'nvr -s -p'
#   abbr e 'nvr -s'
# end

if type -q code
  #echo "✓ VSCode"
  abbr c. 'code (git rev-parse --show-toplevel)'
end

if type -q vmware-user; and type -q sway
  #echo "✓ VMWare Sway"
  abbr S 'env WLR_NO_HARDWARE_CURSORS=1 dbus-launch sway'
end

if type -q swaymsg
  #echo "✓ Sway"
  abbr so 'sway-outputs'
end

if type -q kubectl
  #echo "✓ Kubectl"
  abbr k 'kubectl'
end
