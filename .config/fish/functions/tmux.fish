function tmux
  /bin/tmux has-session -t default && /bin/tmux attach -t default || /bin/tmux -f ~/.config/tmux/.tmux.conf new -s default
end

