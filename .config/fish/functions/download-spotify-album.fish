function download-spotify-album
  for f in (find . -type f -name '*.txt')  
      set folder (string split -r -m2 . (string split -r -m1 / $f))[1]
      mkdir $folder 
      spotdl --list $f -f $folder
      mv $f $folder
  end
end

