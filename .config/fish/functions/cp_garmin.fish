function cp_garmin
  set mount "/mnt/garmin"
  if grep -qs "$mount" /proc/mounts 
    echo "It's mounted."
  else
    echo "It's not mounted."
    mount "$mount"
      if [ $status -eq 0 ]
        echo "Mount success!"
      else
        echo "Something went wrong with the mount..."
        return 1
      end
  end
  set activities "$mount/Garmin/Activities/*.fit"
  set cmd 'mv -f '$activities '/home/czichy/Dokumente/gesundheit/garmin/activities/'
  eval $cmd
  return 0
end

