# https://www.freedesktop.org XDG ENV variable declarations
set -x XDG_CACHE_HOME $HOME/.cache
set -x XDG_CONFIG_HOME $HOME/.config
set -x XDG_DATA_HOME $HOME/.local/share
set -x XDG_CONFIG_DIRS /etc/xdg
set -x XDG_DATA_DIRS "/usr/local/share/:/usr/share/"

# System env "normalization"
# set HOSTNAME, if not set
[ -z "$HOSTNAME-" ] && set -x HOSTNAME=(uname -n)
# set USER, if not set
[ -z "$USER-" ] && set -x USER=(whoami)
# set OSTYPE, if not set
[ -z "$OSTYPE-" ] && set -x OSTYPE="(uname -s)(uname -r)"

# Standart Programme
export BROWSER="brave --force-device-scale-factor=1.44"
export TERMINAL=alacritty
#export TMUX="tmux has-session -t default && $TERMINAL -e tmux attach -t default || $TERMINAL -e tmux -f ~/.config/tmux/.tmux.conf new -s default "

#ssource "$XDG_CONFIG_HOME"/shell/common/en:www_local

# colored man output
# from http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
setenv LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
setenv LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
setenv LESS_TERMCAP_me \e'[0m'           # end mode
setenv LESS_TERMCAP_se \e'[0m'           # end standout-mode
setenv LESS_TERMCAP_so \e'[38;5;246m'    # begin standout-mode - info box
setenv LESS_TERMCAP_ue \e'[0m'           # end underline
setenv LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline

# For RLS
# https://github.com/fish-shell/fish-shell/issues/2456
setenv LD_LIBRARY_PATH (rustc --print sysroot)"/lib:$LD_LIBRARY_PATH"
setenv RUST_SRC_PATH (rustc --print sysroot)"/lib/rustlib/src/rust/src"

setenv FZF_DEFAULT_COMMAND 'rg --files --no-ignore-vcs --hidden'
setenv FZF_CTRL_T_COMMAND 'fd --type file --follow'
setenv FZF_DEFAULT_OPTS '--height 20%'

# Language
setenv  LANG 'de_DE.UTF-8'
setenv  LC_ALL $LANG
setenv  LC_COLLATE $LANG
setenv  LC_CTYPE $LANG
setenv  LC_MESSAGES $LANG
setenv  LC_MONETARY $LANG
setenv  LC_NUMERIC $LANG
setenv  LC_TIME $LANG

# Personal information
setenv NAME 'Christian Czichy'
setenv EMAIL 'Christian@Czichy.com'

# nnn
set -gx NNN_PLUG 'v:imgview;p:imgthumb;i:_sxiv -t .*'
#set DISPLAY (ip route | awk '{print $3; exit}'):0
set DISPLAY :0.0
set LIBGL_ALWAYS_INDIRECT 1
