# dotfiles
pathadd $HOME/.bin

# brew
pathadd /usr/local/sbin

# python
# set `python3` as `python`
pathadd /usr/local/opt/python/libexec/bin
pathadd /home/czichy/.local/bin

# coreutils
set -l coreutils /usr/local/opt/coreutils
pathadd $coreutils/libexec/gnubin

# golang
set -gx GOPATH $HOME/dev/go
set -gx GOBIN $GOPATH/bin
pathadd $GOBIN

# rust
pathadd $HOME/.cargo/bin

# ruby
pathadd $HOME/.gem/ruby/2.7.0/bin

#node
set -gx NVM_DIR $HOME/.config/nvm
